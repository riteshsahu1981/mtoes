<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteBanner extends Model
{
    use HasFactory;
    protected $fillable   = ['title', 'description','site_id', 'banner_image' , 'created_at', 'updated_at'];

}
