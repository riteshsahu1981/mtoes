<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use HasFactory;
    protected $fillable   = ['name','url','logo', 'created_at', 'updated_at', 'twitter_link', 'facebook_link', 
                            'linkedin_link', 'google_link', 'privacy_policy', 'company_short_description', 'company_address'];

    public function pages()
    {
        return $this->hasMany(Page::class);
    }
    public function banners()
    {
        return $this->hasMany(SiteBanner::class);
    }
}
