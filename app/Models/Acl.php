<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acl extends Model
{
    protected $table = 'acl';
    use HasFactory;
    protected $fillable   = ['role_key', 'resource_key','add_permission', 'edit_permission', 'delete_permission', 'view_permission' ,'created_at', 'updated_at'];
}
