<?php
namespace App\Traits;
use Illuminate\Support\Facades\Log;
use App\Models\McPayment;
use App\LogRequest;
trait McPaymentTrait {
	public function createPaymentUrlMcPayment($currency, $totalAmount, $referenceNo, $returnUrl, $statusUrl, $itemDetail, $packprice, $tokenize = "Y", $parentName, $childName, $email, $phone, $level, $country, $category, $class_id, $batch_id) {
        //dd($category);
		try {
			$dataArr = [
				"mcptid"      => env("MCPTID", "3122020041"),
				"currency"    => $currency,
				"totalAmount" => $totalAmount,
				"referenceNo" => $referenceNo,
				"returnUrl"   => $returnUrl,
				"statusUrl"   => $statusUrl,
				"itemDetail"  => $itemDetail,
				"tokenize"    => $tokenize
			];
			$curlInit = curl_init();
			curl_setopt($curlInit, CURLOPT_URL, "https://gw2.mcpayment.net/api/v6/payment");
			curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlInit, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curlInit, CURLOPT_POST, true);
			curl_setopt($curlInit, CURLOPT_POSTFIELDS, http_build_query($dataArr));
			curl_setopt($curlInit, CURLOPT_SSL_VERIFYPEER, false);
			$curlResponse = curl_exec($curlInit);
			$curlInfo     = curl_getinfo($curlInit);
			curl_close($curlInit);
			/*...*/
            LogRequest::paymentMcLog('---MC Payment Transaction---', array("reference_no" => $referenceNo,"request_payload" => var_export($dataArr, true), "response" => $curlResponse));

			McPayment::create([
				"currency"     => $currency,
				"total_amount" => $packprice,
				"reference_no" => $referenceNo,
				"return_url"   => $returnUrl,
				"status_url"   => $statusUrl,
				"item_detail"  => $itemDetail,
				"tokenize"     => $tokenize,
				"response"     => json_encode($curlResponse),
				"created_at"   => date("Y-m-d H:i:s"),
				"updated_at"   => date("Y-m-d H:i:s"),
                "parent_name" => $parentName,
                "email" => $email,
                "phone" => $phone,
                "child_name" => $childName,
                "level" => $level,
                "country" => $country,
                "category" => $category,
                "class_id" => $class_id,
                "batch_id" => $batch_id
			]);

			return $curlResponse;

		} catch (\Exception $_Exception) {
			LogRequest::paymentMcLog('---Mc Charge Error---', array("McPaymentTrait => createPaymentUrlMcPayment : " . $_Exception->getMessage()));
            return false;
		}
	}
}
