<?php

namespace App\Util;

use GuzzleHttp\Client;

class ApiConnect
{

    public static function sendInBlue($method, $methodName, $content_type, $data)
    {
        $client = new Client(); //GuzzleHttp\Client
        $body_data = json_encode($data);
        try {
            $response = $client->$method(env('EWS_SENDINAPI_DOMAIN') . '/' . $methodName,
                array(
                    'headers' => array(
                        'Content-Type' => $content_type,
                        'api-key' => env('SENDINBLUE_KEY'),
                        'partner-key' => env('SENDINBLUE_KEY'),
                    ),
                    'body' => $body_data,
                )
            );
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return 0;
        }
    }

    public static function sendInBlueEmail($method, $methodName, $content_type, $data)
    {
        $client = new Client(); //GuzzleHttp\Client
        $body_data = json_encode($data);
        try {
            $response = $client->$method(env('EWS_SENDINAPI_DOMAIN') . '/' . $methodName,
                array(
                    'headers' => array(
                        'Content-Type' => $content_type,
                        'api-key' => env('SENDINBLUE_KEY'),
                        'partner-key' => env('SENDINBLUE_KEY'),
                    ),
                    'body' => $body_data,
                )
            );
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return false;
            //return $e->getMessage();
        }
    }

    public static function sendInBlueEmailJsonResponse($method, $methodName, $content_type, $data)
    {
        $client = new Client(); //GuzzleHttp\Client
        $body_data = json_encode($data);
        $result = array();

        try {
            $response = $client->$method(env('EWS_SENDINAPI_DOMAIN') . '/' . $methodName,
                array(
                    'headers' => array(
                        'Content-Type' => $content_type,
                        'api-key' => env('SENDINBLUE_KEY'),
                        'partner-key' => env('SENDINBLUE_KEY'),
                    ),
                    'body' => $body_data,
                )
            );

            $result['data'] =  json_decode($response->getBody(),true);
            $result['status'] = 'success';
            return $result;
        } catch (\Exception $e) {
            $result['status'] = 'error';
            return $result;
        }
    }

    public static function sendGridEmail($data)
    {
        return self::sendGridAPICall("/v3/mail/send", "post", $data);
    }

    public static function sendGridAddContactsToContactList($data)
    {
        return self::sendGridAPICall("/v3/marketing/contacts", "put", $data);
    }

    public static function sendGridAPICall($endpoint, $method, $data)
    {
        $client = new Client(); //GuzzleHttp\Client
        $body_data=json_encode($data);
        //dd($body_data);
        try {
            $response = $client->$method(env('SENDGRID_API_DOMAIN').$endpoint,
                array(
                    'headers' => array(
                        'Content-Type' => "application/json",
                        'Authorization' => "Bearer ".env('SENDGRID_API_KEY')
                    ),
                    'body' => $body_data,
                )
            );

            return $response->getReasonPhrase();
            //return json_decode($response->getBody());
        } catch (\Exception $e) {
            return false;
            //return $e->getMessage();
        }
    }

    public static function registerB2BUser($data)
    {
        $endpoint="/api/b2b/register";
        $client = new Client(); //GuzzleHttp\Client
        $body_data=json_encode($data);
        //dd($body_data);
        try {
            $response = $client->post(env('B2B_API_DOMAIN').$endpoint,
                array(
                    'headers' => array(
                        'Content-Type' => "application/json",
                    ),
                    'body' => $body_data,
                )
            );

            //return $response->getReasonPhrase();
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return false;
            //return $e->getMessage();
        }
    }

}
