<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Site;

class PageController extends Controller
{

    public function page($identifier)
    {
        $siteName="Default";
        $site=Site::where(['url'=>$this->getSiteUrl()])->first();
        if(!is_null($site)){
            $siteName=$site->name;
        }else{
            return view('maintenance');
        }
        $page=$site->pages()->where('identifier', $identifier)->first();
        $pageTitle=$this->getPageTitleByIdenfier($identifier);
       
        $metaTitle=isset($page) && !is_null($page->meta_title)? $page->meta_title: "Page Title";
        $metaDescription=isset($page) && !is_null($page->meta_description)? $page->meta_description: "";
        $metaKeywords=isset($page) && !is_null($page->meta_keywords)? $page->meta_keywords: "";

        view()->share('selectedMenuItem', $identifier);
        view()->share('pageName', $pageTitle);
        return view('page.page', ['metaTitle'=>$metaTitle, 'metaDescription'=>$metaDescription, 'metaKeywords'=>$metaKeywords, 'pageTitle' => $siteName.' | '.$pageTitle, 'page'=>$page, 'pageName'=>$pageTitle, 'identifier'=>$identifier]);
    }
}
