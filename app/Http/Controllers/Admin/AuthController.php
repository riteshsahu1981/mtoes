<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        //dd(Auth::check());
        if(Auth::check()){
            return redirect('/admin');
        }
        return view('admin.auth.login', ['pageTitle' => 'Login']);
    }

    public function authenticate(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
            // Authentication passed...
            //return redirect()->intended('dashboard');
            return response()->json(array("status"=>"success", "message"=>"Login succeeded."));
        }else{
            //echo "authentication faileed";
            //return redirect()->intended('login');
            return response()->json(array("status"=>"error", "message"=>"Invalid credentials.Please try again."));
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/admin/login');
    }

}
