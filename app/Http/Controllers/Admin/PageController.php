<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Site;

class PageController extends Controller
{
    public function page($identifier, $site_id=null)
    {


        $mySites=Helper::getMySites();
        if(is_null($site_id)){
            $site_id=$mySites[0]->id;
        }


        $site=Site::where('id', $site_id)->first();
        if(is_null($site)){
            echo "Invalid site.";
            return false;
        }
        $page=$site->pages()->where('identifier', $identifier)->first();
        $pageTitle=$this->getPageTitleByIdenfier($identifier);
        view()->share('selectedMenuItem', $identifier);
        return view('admin.page.manage-page', ['mySites'=>$mySites,  'pageTitle' => 'Admin | Manage '.$pageTitle.' Page', 'page'=>$page, 'site'=>$site, 'pageName'=>$pageTitle, 'identifier'=>$identifier]);
    }

    public function savePage(Request $request)
    {
        $page=Page::where(['id'=>$request->page_id])->first();
        if(is_null($page)){
            $data=[
                'site_id'=>$request->site_id,
                'content'=>$request->content,
                'meta_title'=>$request->meta_title,
                'meta_keywords'=>$request->meta_keywords,
                'meta_description'=>$request->meta_description,
                'identifier' => $request->identifier,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $page=Page::create($data);
        }else{
            $page->updated_at=date('Y-m-d H:i:s');
            $page->content=$request->content;
            $page->meta_title=$request->meta_title;
            $page->meta_keywords=$request->meta_keywords;
            $page->meta_description=$request->meta_description;
            $page->save();
        }
        return response()->json(array("status"=>"success", "message"=> 'Page content save succesfully against page id '. $page->id, 'page_id'=>$page->id) );
    }

}
