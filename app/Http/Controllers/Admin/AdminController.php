<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Site;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;
class AdminController extends Controller
{
    public function __construct(){
        parent::__construct();
        //dd(Auth::check());
        // if(Auth::check() == false){
        //     return redirect('/admin/login');
        // }
    }

    public function dashboard(Request $request)
    {
        $user = Auth::user();
        //dd($user->role_key);
        $siteName="Default";

        $site=Site::where(['url'=>$this->getSiteUrl()])->first();
        if(!is_null($site)){
            $siteName=$site->name;
        }
        view()->share('selectedMenuItem', 'dashboard');
        return view('admin.dashboard', ['pageTitle' => 'Explico | Dashboard']);
    }

    public function sites(Request $request)
    {
        if(!Helper::checkPermission(Auth::user()->role_key, 'site', 'view')){
            view()->share('selectedMenuItem', 'sites');
            return view('admin.unauthorized', ['pageTitle' => '403-Unauthorized']);

        }
        $sites=Site::get();
        view()->share('selectedMenuItem', 'sites');
        return view('admin.sites', ['pageTitle' => 'Explico | Sites', 'sites' => $sites]);
    }

    public function getSites(Request $request){
        $offset=$request->start;
        $limit=$request->length;
        $searchText=$request->search['value']   ;
        $sortOrderColumnIndex=$request->order[0]['column'];
        $sortOrder=$request->order[0]['dir'];
        $columns=$request->columns;
        $filter=array();
        //$sortColumnName="name";
        //$sortOrder="asc";

        $query=Site::offset($offset)
                        ->limit($limit);


        foreach($columns as $_column){
            if($_column['data']==$sortOrderColumnIndex){
               //$sortColumnName=$_column['name'];
                $query->orderBy($_column['name'], $sortOrder);
            }
            if($searchText!="" && $_column['searchable']=="true"){
                $query->orWhere($_column['name'], 'like', "%".$searchText."%");
            }
        }
        //dd($query->toSql());
        $result=$query->get();

        $finalArray['data']=array();
        foreach($result as $row){
            $finalArray['data'][]=array(
                $row['id'],
                $row['name'],
                $row['url'],
                is_null($row['logo']) ? "N/A":'<img width="173" height="30" src="'.asset("uploads/logo/".$row['logo']).'? '.rand(1,100).'">',
                "<a class='btn btn-info btn-sm' href='".url('/admin/manageSite/'.$row['id'])."'>Edit</a>"
            );
        }
        $finalArray['draw']=$request->draw;
        $finalArray['recordsTotal']=Site::count();
        $finalArray['recordsFiltered']=Site::orWhere($filter)->count();

        return response(json_encode($finalArray), 200)
                  ->header('Content-Type', 'application/json');
    }

    public function manageSite($id=null){
        $pageSubTitle='Edit Site';
        $site=Site::where(['id'=>$id])->first();
        if(is_null($site)){
            $pageSubTitle='Add Site';
        }
        view()->share('selectedMenuItem', 'sites');
        return view('admin.manage-site', ['pageTitle' => "Explico | ".$pageSubTitle, 'pageSubTitle'=>$pageSubTitle, 'id'=>$id, 'site'=>$site]);
    }

    public function saveSite(Request $request)
    {
        $request->validate([
            'siteLogo' =>'file|mimes:jpeg,jpg,png|max:10240',
            'name' => 'required',
            'url' => 'required',
            'twitter_link'=>'sometimes|nullable',
            'facebook_link'=>'sometimes|nullable',
            'linkedin_link'=>'sometimes|nullable',
            'google_link'=>'sometimes|nullable',
            'privacy_policy'=>'sometimes|nullable',
            'company_short_description'=>'sometimes|nullable',
            'company_address'=>'sometimes|nullable',
            'website_title'=>'sometimes|nullable',
            'meta_description'=>'sometimes|nullable',
            'meta_keywords'=>'sometimes|nullable',
            'site_admin_email'=>'sometimes|nullable'

        ]);

        //dd($request->cropSiteLogo);
        $siteData=['name'=>$request->name, 
                    'url'=>$request->url,
                    'twitter_link'=>$request->twitter_link,
                    'facebook_link'=>$request->facebook_link,
                    'linkedin_link'=>$request->linkedin_link,
                    'google_link'=>$request->google_link,
                    'privacy_policy'=>$request->privacy_policy,
                    'company_short_description'=>$request->company_short_description,
                    'company_address'=>$request->company_address,
                    'website_title'=>$request->website_title,
                    'meta_description'=>$request->meta_description,
                    'meta_keywords'=>$request->meta_keywords,
                    'site_admin_email'=>$request->site_admin_email
                ];
        if(is_null($request->id)){//add
            $siteData['created_at']=date("Y-m-d H:i:s");
            $site=Site::create($siteData);
            $site_id=$site->id;
        }else{//update
            $site_id=$request->id;
            $site=Site::where(['id'=>$site_id])->first();
            $site->name=$request->name;
            $site->url=$request->url;
            $site->twitter_link=$request->twitter_link;
            $site->facebook_link=$request->facebook_link;
            $site->linkedin_link=$request->linkedin_link;
            $site->google_link=$request->google_link;
            $site->privacy_policy=$request->privacy_policy;
            $site->company_short_description=$request->company_short_description;
            $site->company_address=$request->company_address;
            $site->website_title=$request->website_title;
            $site->meta_description=$request->meta_description;
            $site->meta_keywords=$request->meta_keywords;
            $site->site_admin_email=$request->site_admin_email;
            $siteData['updated_at']=date("Y-m-d H:i:s");
        }

        if(!empty($request->file('favicon'))){
            $faviconFileName = 'site_id_'.$site_id.'_favicon.'.$request->favicon->extension();
            $site->favicon=$faviconFileName;
            // $arrImage=explode(',', $request->cropSiteLogo);
            // $data=base64_encode($arrImage[1]);
            // Storage::put('logo/'.$fileName, $data);
            $request->favicon->move(public_path('uploads/favicon'), $faviconFileName);
        }
        if(!empty($request->cropSiteLogo)){
            //$site->logo=$request->cropSiteLogo;
            $fileName = 'site_id_'.$site_id.'_logo.'.$request->siteLogo->extension();
            $image_parts = explode(";base64,", $request->cropSiteLogo);
            $image_type_aux = explode("image/", $image_parts[0]);
            //$image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            file_put_contents(public_path('uploads/logo/').$fileName, $image_base64);
            $site->logo=$fileName;
        }
        $site->save();
        return redirect('/admin/manageSite/'.$site_id)
                ->with('success','Site information saved successfully.');

    }

    public function header(Request $request)
    {
        $siteName="Default";
        $site=Site::where(['url'=>$this->getSiteUrl()])->first();
        if(!is_null($site)){
            $siteName=$site->name;
        }
        view()->share('selectedMenuItem', 'header');
        return view('admin.header', ['pageTitle' => 'Explico | Header']);
    }

    public function footer(Request $request)
    {
        $siteName="Default";
        $site=Site::where(['url'=>$this->getSiteUrl()])->first();
        if(!is_null($site)){
            $siteName=$site->name;
        }
        view()->share('selectedMenuItem', 'footer');
        return view('admin.footer', ['pageTitle' => 'Explico | Footer']);
    }




}
