<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\Role;
use App\Models\Site;
use App\Models\UserSite;
use Exception;

class UserController extends Controller
{
    public function users(Request $request)
    {
        $mySites=Helper::getMySites();
        view()->share('selectedMenuItem', 'users');
        return view('admin.user.users', ['pageTitle' => 'Admin | Users List', 'pageName'=>'Users', 'mySites'=>$mySites]);
    }
    public function getUsers(Request $request)
    {
        $site_id=$request->site_id;
        $offset=$request->start;
        $limit=$request->length;
        $searchText=$request->search['value']   ;
        $sortOrderColumnIndex=$request->order[0]['column'];
        $sortOrder=$request->order[0]['dir'];
        $columns=$request->columns;
        $filter=array();

        $query=User::select('users.*')->offset($offset)
                        ->limit($limit)
                        ->join('user_sites', function($join ) use ($site_id){
                            $join->on('users.id', '=', 'user_sites.user_id')
                            ->where('user_sites.site_id', $site_id);
                        });
                        //->join('user_sites','users.id', '=', 'user_sites.user_id')
                        //->where('user_sites.site_id', $site_id);


        foreach($columns as $_column){
            if($_column['data']==$sortOrderColumnIndex){
               //$sortColumnName=$_column['name'];
                $query->orderBy("users.".$_column['name'], $sortOrder);
            }
            if($searchText!="" && $_column['searchable']=="true"){
                $query->orWhere("users.".$_column['name'], 'like', "%".$searchText."%");
            }
        }
        //dd($query->toSql());
        $result=$query->get();

        $finalArray['data']=array();
        foreach($result as $row){
            $finalArray['data'][]=array(
                $row['id'],
                $row['name'],
                $row['email'],
                $row['role_key'],
                "<a class='btn btn-info btn-sm' href='".url('/admin/user/manageUser/'.$row['id'])."'>Edit</a>"
            );
        }
        $finalArray['draw']=$request->draw;
        $finalArray['recordsTotal']=User::join('user_sites', function($join ) use ($site_id){
                                                $join->on('users.id', '=', 'user_sites.user_id')
                                                ->where('user_sites.site_id', $site_id);
                                        }) 
                                        ->count();
        $finalArray['recordsFiltered']=User::orWhere($filter)
                                        ->join('user_sites', function($join ) use ($site_id){
                                            $join->on('users.id', '=', 'user_sites.user_id')
                                            ->where('user_sites.site_id', $site_id);
                                        })  
                                        ->count();

        return response(json_encode($finalArray), 200)
                  ->header('Content-Type', 'application/json');
    }
    public function manageUser($user_id=null)
    {
        $pageLabel="Add New User";
        $userSites=array();
        $user=null;
        if(!is_null($user_id)){
            $pageLabel="Edit User";
            $user=User::where('id', $user_id)->first();
            if(is_null($user)){
                echo "Invalid user id.";
                return false;
            }
            $userSites=UserSite::where('user_id', $user_id)->get();
        }
        view()->share('selectedMenuItem', 'users');
        $roles=Role::where('role_key','<>', 'super_admin')->get();
        $sites=Site::get();
        
        return view('admin.user.manage-user', ['userSites'=>$userSites,'sites'=>$sites, 'roles'=>$roles, 'pageLabel'=>$pageLabel, 'pageTitle' => 'Admin | Manage User', 'user'=>$user]);
    }

    public function saveUser(Request $request)
    {
        $user=User::where(['id'=>$request->user_id])->first();
         
        
        if(is_null($user)){
            $data=[
                'name'=>$request->name,
                'role_key'=>$request->role_key,
                'email'=>$request->email,
                'password'=>$request->password,
                'created_at' => date('Y-m-d H:i:s')
            ];
            try{
                $user=User::create($data);
            }catch(Exception $e){

        return response()->json(array("status"=>"error", "message"=> $e->getMessage()) );
            }
            
        }else{
            $user->updated_at=date('Y-m-d H:i:s');
            $user->name=$request->name;
            if(!is_null($request->password) && $request->password !=""){
                $user->password=$request->password;
            }
            $user->role_key=$request->role_key;
            $user->save();
        }

        if(!is_null($request->site)){
            UserSite::where(['user_id'=>$user->id])->delete();
            $arrSite=explode(',', $request->site);
            foreach($arrSite as $site_id){
                UserSite::create(['user_id'=>$user->id, 'site_id'=>$site_id]);
            }
        }
       
        return response()->json(array("status"=>"success", "message"=> 'User information saved succesfully against user id '. $user->id, 'user_id'=>$user->id) );
    }

}
