<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\SiteBanner;

class BannerController extends Controller
{
    public function banners(Request $request)
    {

        $mySites=Helper::getMySites();
        //$site_id=$mySites[0]->id;

        // $site=Site::where('id', $site_id)->first();
        // if(is_null($site)){
        //     echo "Invalid site.";
        //     return false;
        // }
        view()->share('selectedMenuItem', 'banners');
        return view('admin.banner.banners', ['mySites'=>$mySites,  'pageTitle' => 'Admin | Manage Site Banner',    'pageName'=>'Banners']);
    }

    public function manageBanner($site_id=null, $banner_id=null)
    {
        $mySites=Helper::getMySites();
        if(is_null($site_id)){
            $site_id=$mySites[0]->id;
        }


        $site=Site::where('id', $site_id)->first();
        if(is_null($site)){
            echo "Invalid site.";
            return false;
        }
        $banner=null;
        $pageLabel="Add Banner Content";
        if(!is_null($banner_id)){
            $pageLabel="Edit Banner Content";
            $banner=SiteBanner::where('id', $banner_id)->first();
            if(is_null($banner)){
                echo "Invalid banner id.";
                return false;
            }
        }
        view()->share('selectedMenuItem', 'banners');
        return view('admin.banner.manage-banner', [  'pageLabel'=>$pageLabel, 'mySites'=>$mySites,  'pageTitle' => 'Admin | Manage Banner', 'banner'=>$banner, 'site'=>$site]);
    }

    public function getBanners(Request $request)
    {
        $site_id=$request->site_id;
        $offset=$request->start;
        $limit=$request->length;
        $searchText=$request->search['value']   ;
        $sortOrderColumnIndex=$request->order[0]['column'];
        $sortOrder=$request->order[0]['dir'];
        $columns=$request->columns;
        $filter=array();
        //$sortColumnName="name";
        //$sortOrder="asc";

        $query=SiteBanner::offset($offset)
                        ->limit($limit)
                        ->where('site_id',$site_id);


        foreach($columns as $_column){
            if($_column['data']==$sortOrderColumnIndex){
               //$sortColumnName=$_column['name'];
                $query->orderBy($_column['name'], $sortOrder);
            }
            if($searchText!="" && $_column['searchable']=="true"){
                $query->orWhere($_column['name'], 'like', "%".$searchText."%");
            }
        }
        //dd($query->toSql());
        $result=$query->get();

        $finalArray['data']=array();
        foreach($result as $row){
            $finalArray['data'][]=array(
                $row['id'],
                $row['title'],
                is_null($row['banner_image']) ? "N/A":'<img width="173" height="30" src="'.asset('/uploads/site_banners/'.$row['banner_image']).'">',
                "<a class='btn btn-info btn-sm' href='".url('/admin/banners/manageBanner/'.$row['site_id']."/".$row['id'])."'>Edit</a>  <button banner_id='".$row['id']."' class='btn btn-danger btn-sm cmdDeleteBanner'>Delete</buton>"
            );
        }
        $finalArray['draw']=$request->draw;
        $finalArray['recordsTotal']=SiteBanner::count();
        $finalArray['recordsFiltered']=SiteBanner::orWhere($filter)->where('site_id', $site_id)->count();

        return response(json_encode($finalArray), 200)
                  ->header('Content-Type', 'application/json');
    }

    public function saveSiteBanner(Request $request)
    {
        $request->validate([
            'banner_image' =>'file|mimes:jpeg,jpg,png|max:10240',
            'title' => 'required',
            'description' => 'required'
        ]);

        $data=['title'=>$request->title, 'description'=>$request->description, 'site_id'=>$request->site_id];
        if(is_null($request->banner_id)){//add
            $siteData['created_at']=date("Y-m-d H:i:s");
            $banner=SiteBanner::create($data);
            $banner_id=$banner->id;
        }else{//update
            $banner_id=$request->banner_id;
            $banner=SiteBanner::where(['id'=>$banner_id])->first();
            $banner->title=$request->title;
            $banner->description=$request->description;
            $data['updated_at']=date("Y-m-d H:i:s");
        }

        if(!empty($request->file('banner_image'))){
            $fileName = 'banner_id_'.$banner_id.'_'.time().'.'.$request->banner_image->extension();
            $banner->banner_image=$fileName;
            $request->banner_image->move(public_path('uploads/site_banners'), $fileName);
        }
        $banner->save();
        return redirect('/admin/banners/manageBanner/'.$request->site_id."/".$banner_id)
                ->with('success','Site Banner saved successfully.');

    }

    public function deleteBanner(Request $request)
    {
        //dd($request->input('banner_id'));
        $status=SiteBanner::where('id', $request->post( 'banner_id'))->delete();
        $finalArray=array("status"=>"fail", "message"=>"Unable to delete the banner!");
        if($status){
            $finalArray=array("status"=>"success", "message"=>"Banner deleted successfully!");
        }
        return response(json_encode($finalArray), 200)
                  ->header('Content-Type', 'application/json');
    }

}
