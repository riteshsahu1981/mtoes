<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Site;

class HomeController extends Controller
{

    public function home(Request $request)
    {
        $siteName="Default";
        $websiteTitle="";
        $site=Site::where(['url'=>$this->getSiteUrl()])->first();
        if(!is_null($site)){
            $siteName=$site->name;
            $websiteTitle=$site->website_title;
            $metaKeywords=$site->meta_keywords;
            $metaDescription=$site->meta_description;
        }else{

            return view('maintenance');

        }
        view()->share('selectedMenuItem', 'home');
        view()->share('pageName', 'Home');

        return view('home.home', ['websiteTitle'=>$websiteTitle, 'metaDescription'=>$metaDescription, 'metaKeywords'=>$metaKeywords,'pageTitle' => $siteName.' | Home']);
    }
}
