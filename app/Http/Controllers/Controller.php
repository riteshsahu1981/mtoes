<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Site;
use App\Helpers\Helper;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $site=Helper::getCurrentSite();
        $logo=asset("assets/images/logo.png");
        if(!is_null($site) && !is_null($site->logo)){
            $logo=asset("uploads/logo/".$site->logo).'?'.rand(1,100);
            //$logo=$site->logo;
        }
        $favicon=asset("admin_assets/dist/img/explico-cmyk-logomark@2x.png");
        if(!is_null($site) && !is_null($site->favicon)){
            $favicon=asset("uploads/favicon/".$site->favicon).'?'.rand(1,100);
            //$logo=$site->logo;
        }
        //dd($logo);
        // $faqPage=$site->pages()->where('identifier', "faq")->first();
        // $contactusPage=$site->pages()->where('identifier', "contactus")->first();
        // $aboutusPage=$site->pages()->where('identifier', "aboutus")->first();
        // view()->share('faqPage', $faqPage);
        // view()->share('contactusPage', $contactusPage);
        // view()->share('aboutusPage', $aboutusPage);
        view()->share('logo', $logo);
        view()->share('favicon', $favicon);
        view()->share('currentSite', $site);

    }
    public  function generateRandomCode($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getSiteUrl(){
        return Helper::getSiteUrl();
    }
    public function getPageTitleByIdenfier($identifier){
        $pageTitle=[
            'aboutus'=>'About Us',
            'contactus'=>'Contact Us',
            'faq' =>'FAQ'
        ];
        return $pageTitle[$identifier];
    }
}
