<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\SiteBanner;
use App\Helpers\Helper;
class BannerSlider extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $site=Helper::getCurrentSite();
        $banners=SiteBanner::where(['site_id'=>$site->id])->get();
        //dd($banners->count());
        return view('components.banner-slider', ['banners'=>$banners]);
    }
}
