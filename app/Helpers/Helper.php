<?php

namespace App\Helpers;
use App\Models\Acl;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Site;
class Helper
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function checkPermission($roleKey, $resourceKey, $permission)
    {
        $arrPermissionColumns=array('add'=>'add_permission', 'edit'=>'edit_permission', 'delete'=>'delete_permission', 'view'=>'view_permission' );
        $acl=Acl::where(['role_key'=>$roleKey, 'resource_key'=>$resourceKey, $arrPermissionColumns[$permission]=>"1"])->first();
        return is_null($acl) ? false : true;
    }

    public static function getMySites()
    {
        if(!Auth::check()){
            return false;
        }
        $user = Auth::user();
        $sites=false;
        if($user->role_key=='super_admin'){
            $sites = DB::table('sites')
            ->select('*')
            ->get();
        }else{
            $sites = DB::table('user_sites')
            ->join('sites', 'user_sites.site_id', '=', 'sites.id')
            ->select('sites.*')
            ->where('user_sites.user_id', $user->id)
            ->get();
        }
        return $sites;

    }

    public function getCurrentSite(){
        return $site=Site::where(['url'=>self::getSiteUrl()])->first();
    }

    public function getSiteUrl(){
        return url('/');
    }
}
