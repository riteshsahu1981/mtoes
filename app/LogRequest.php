<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class LogRequest extends Model
{
    //
    public static function logRequest($type,$data)
	{
		\Log::channel('api_response_log')->info($type , $data);
	}
	public static function paymetnLogRequest($type,$data)
	{
		\Log::channel('payment')->info($type , $data);
	}
	public static function sessionLogRequest($type,$data)
	{
		\Log::channel('session')->info($type , $data);
	}
	public static function facematchLogRequest($type,$data)
	{
		\Log::channel('face_match')->info($type , $data);
	}
	public static function videoConversionRequest($type,$data)
	{
		\Log::channel('video_conversion_request')->info($type , $data);
	}

	public static function eventXmlRequest($type,$data)
	{
		\Log::channel('event_xml_request')->info($type , $data);
	}

	public static function assignmentRequest($type,$data)
	{
		\Log::channel('assignment_request')->info($type , $data);
	}

	public static function yearUpgradeRequest($type,$data)
	{
		\Log::channel('year_upgrade')->info($type , $data);
	}

	public static function submitExamCron($type,$data)
	{
		\Log::channel('submit_exam_cron')->info($type , $data);
	}

	public static function submitSessionInterestCron($type,$data)
	{
		\Log::channel('submit_session_interest_count')->info($type , $data);
	}

	public static function branchIo($type,$data)
	{
		\Log::channel('branch_io')->info($type , $data);
	}

	public static function recordingDelete($type,$data)
	{
		\Log::channel('recording_delete')->info($type , $data);
	}

	public static function paymentMcLog($type,$data)
	{
		\Log::channel('paymentMc')->info($type , $data);
	}
	public static function deadlineLogs($type,$data)
	{
		\Log::channel('deadline_response')->info($type , $data);
	}
	public static function b2bAPILog($type,$data)
	{
		\Log::channel('b2bAPILog')->info($type , $data);
	}
}
