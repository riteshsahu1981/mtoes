<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <script src="{{ asset('admin_assets/plugins/jquery-validation/additional-methods.js') }}"></script>
        <script src="{{ asset('admin_assets/dist/js/pages.js?v=1') }}"></script>
    @endsection
    @section('customHead')
        <meta name="csrf_token" content="{{ csrf_token() }}" />
    @endsection

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">

                <select class="custom-select" id="comboSiteId" name="comboSiteId">
                    @foreach ($mySites as $_mySite)
                        <option value="{{$_mySite->id}}" @if($site->id ==$_mySite->id ) selected="selected" @endif >{{$_mySite->name}} - {{$_mySite->url}}</option>
                    @endforeach
                </select>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{$pageName}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
    <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Enter {{$pageName}} Page Content</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="frmManagePage" method="POST" action="{{url("/admin/site/save")}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">

                                    <textarea id="content" name="content" class="form-control" rows="10" > {{isset($page->content) ? $page->content : ""}}</textarea>

                                    <input type="hidden" class="form-control" id="site_id" name="site_id" value="{{isset($site->id) ? $site->id : ""}}">
                                    <input type="hidden" class="form-control" id="page_id" name="page_id" value="{{isset($page->id) ? $page->id : ""}}">
                                    <input type="hidden" class="form-control" id="identifier" name="identifier" value="{{$identifier}}">
                                </div>
                                <div class="form-group">
                                    <label for="meta_title">Page Meta Title</label>
                                    <input type="text" class="form-control" id="meta_title" name="meta_title"  placeholder="Enter Page Meta Title" value="{{isset($page->meta_title) ? $page->meta_title : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Page Meta Description</label>
                                    <textarea class="form-control" id="meta_description" name="meta_description"  placeholder="Enter Page Meta Description" >{{isset($page->meta_description) ? $page->meta_description : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Page Meta Keywords</label>
                                    <textarea class="form-control" id="meta_keywords" name="meta_keywords"  placeholder="Enter Page Meta Keywords" >{{isset($page->meta_keywords) ? $page->meta_keywords : ""}}</textarea>
                                </div>


                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" id="cmdSavePage" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->

                </div>
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div id="divMessage" style="display:none;"></div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </x-admin-layout>
