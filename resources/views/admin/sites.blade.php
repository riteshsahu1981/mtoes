<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/dist/js/sites.js') }}"></script>
    @endsection

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-6">
                     <a href="{{url("/admin/manageSite")}}" class="btn btn-primary "><i class="fas fa-plus"></i> Add New Site</a>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url("/admin")}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Sites</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->

            <!-- Main row -->

            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Sites Listing</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tblSites" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Site Id</th>
                                        <th>Site Name</th>
                                        <th>Url</th>
                                        <th>Logo</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @foreach ($sites as $_site)
                                        <tr>
                                            <td>{{ $_site->id }}</td>
                                            <td>{{ $_site->name }}</td>
                                            <td>{{ $_site->url }}</td>
                                            <td>Edit</td>
                                        </tr>
                                    @endforeach

                                </tbody> --}}

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</x-admin-layout>
