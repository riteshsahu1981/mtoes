<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <script src="{{ asset('admin_assets/plugins/jquery-validation/additional-methods.js') }}"></script>
        <script src="{{ asset('admin_assets/dist/js/banners.js') }}"></script>
    @endsection
    @section('customHead')
        <meta name="csrf_token" content="{{ csrf_token() }}" />
    @endsection

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div>

          <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{url("/admin/banners")}}" class="btn btn-primary">Back to Banners</a>
                @if (isset($banner->id))
                <a href="{{url("/admin/banners/manageBanner/".$site->id)}}" class="btn btn-primary"><i class="fas fa-plus"></i> Add New Banner</a>
                @endif

                {{-- <select class="custom-select" id="comboSiteIdManageBanner" name="comboSiteIdManageBanner">
                    @foreach ($mySites as $_mySite)
                        <option value="{{$_mySite->id}}" @if($site->id ==$_mySite->id ) selected="selected" @endif >{{$_mySite->name}} - {{$_mySite->url}}</option>
                    @endforeach
                </select> --}}
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{$pageLabel}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
    <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{$pageLabel}} - {{$site->name}} - {{$site->url}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="frmManageBanner" method="POST" action="{{url("/admin/banners/save")}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Banner Title</label>
                                    <input type="text" class="form-control" id="title" name="title" value="{{isset($banner->title) ? $banner->title : ""}}">

                                    <input type="hidden" class="form-control" id="site_id" name="site_id" value="{{isset($site->id) ? $site->id : ""}}">
                                    <input type="hidden" class="form-control" id="banner_id" name="banner_id" value="{{isset($banner->id) ? $banner->id : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Banner Description</label>
                                    <textarea id="description" name="description" class="form-control" rows="5" > {{isset($banner->description) ? $banner->description : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Banner Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" id="banner_image" name="banner_image">
                                        </div>
                                    </div>
                                </div>
                                @if(isset($banner->banner_image))
                                <div class="form-group">
                                    <img width="173" height="30" src="{{asset("uploads/site_banners/".$banner->banner_image)}}">
                                </div>
                                @endif


                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" id="cmdSavePage" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->

                </div>
            </div><!-- /.row -->


        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </x-admin-layout>
