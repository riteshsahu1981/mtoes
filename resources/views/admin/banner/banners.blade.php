<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/dist/js/banners.js') }}"></script>
    @endsection

    @section('customHead')
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    @endsection
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-3">
                     <button class="btn btn-primary" id="cmdAddBanner" name="cmdAddBanner"><i class="fas fa-plus"></i> Add New Banner</button >
                </div>
                <div class="col-sm-6">
                    <select class="custom-select" id="comboSiteId" name="comboSiteId">
                        @foreach ($mySites as $_mySite)
                            <option value="{{$_mySite->id}}"   >{{$_mySite->name}} - {{$_mySite->url}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url("/admin")}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Banners</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->

            <!-- Main row -->

            <div class="row">
                <div class="col-12">

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Banners Listing</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tblBanners" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Site Id</th>
                                        <th>Banner Title</th>
                                        <th>Banner Image</th>
                                        <th></th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</x-admin-layout>
