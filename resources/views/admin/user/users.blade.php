<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/dist/js/users.js') }}"></script>
    @endsection

    @section('customHead')
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    @endsection
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-3">
                     <a  class="btn btn-primary"  href="{{url('admin/user/manageUser')}}"><i class="fas fa-plus"></i> Add New User</a >
                </div>
                <div class="col-sm-6">
                    <select class="custom-select" id="comboSiteId" name="comboSiteId">
                        @foreach ($mySites as $_mySite)
                            <option value="{{$_mySite->id}}"   >{{$_mySite->name}} - {{$_mySite->url}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url("/admin")}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">{{$pageName}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->

            <!-- Main row -->

            <div class="row">
                <div class="col-12">

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{$pageName}} Listing</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tblUsers" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th></th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</x-admin-layout>
