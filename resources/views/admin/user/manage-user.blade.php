<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')
        <script src="{{ asset('admin_assets/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <script src="{{ asset('admin_assets/plugins/jquery-validation/additional-methods.js') }}"></script>
        <script src="{{ asset('admin_assets/dist/js/users.js?v=11') }}"></script>
    @endsection
    @section('customHead')
        <meta name="csrf_token" content="{{ csrf_token() }}" />
    @endsection

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">

            

          <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{url("/admin/users")}}" class="btn btn-primary">Back to Users</a>
                 

                {{-- <select class="custom-select" id="comboSiteIdManageBanner" name="comboSiteIdManageBanner">
                    @foreach ($mySites as $_mySite)
                        <option value="{{$_mySite->id}}" @if($site->id ==$_mySite->id ) selected="selected" @endif >{{$_mySite->name}} - {{$_mySite->url}}</option>
                    @endforeach
                </select> --}}
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{$pageLabel}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
    <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{$pageLabel}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="frmUser" method="POST" action="{{url("/admin/user/save")}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Full Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{isset($user->name) ? $user->name : ""}}">
                                    <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{isset($user->id) ? $user->id : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" @if (isset($user->email))
                                        readonly
                                    @endif value="{{isset($user->email) ? $user->email : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Password</label>
                                    <input type="text" class="form-control" id="password" name="password" value="">
                                </div>
                                <div class="form-group">
                                    <label for="role_key">Role</label>
                                    <select class="custom-select" id="role_key" name="role_key">
                                        @foreach ($roles as $_role)
                                             
                                            <option value="{{$_role->role_key}}" @if(isset($user->role_key) && $_role->role_key == $user->role_key ) selected="selected" @endif    > {{$_role->description}} </option>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="email">Sites</label>
                                    <select multiple class="custom-select" id="site" name="site[]">
                                        @foreach ($sites as $_site)
                                            <option value="{{$_site->id}}" 
                                                @foreach ($userSites as $_userSites )
                                                    @if($_userSites->site_id == $_site->id ) selected="selected" @endif  
                                                @endforeach
                                                
                                                
                                                >{{$_site->name}} - {{$_site->url}} </option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" id="cmdSavePage" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->

                </div>
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                    <div id="divMessage" style="display:none;"></div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </x-admin-layout>
