<x-admin-layout>

    @section('title', $pageTitle)
    @section('customScript')

        <script src="{{ asset('admin_assets/dist/js/cropzee.js?v=7') }}"></script>
        <script src="{{ asset('admin_assets/dist/js/sites.js?v='.rand(1,100)) }}"></script>
    @endsection

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-6">
                    {{-- <h1 class="m-0"><button type="button" class="btn btn-primary "><i class="fas fa-plus"></i> Add New Site</button></h1> --}}
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/sites') }}">Sites</a></li>
                        <li class="breadcrumb-item active">{{ $pageSubTitle }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->




   	{{-- <label for="cropzee-input" class="image-previewer" data-cropzee="cropzee-input"></label>
	<input id="cropzee-input" type="file" accept="image/*">
 <button onclick="cropzeeGetImage('cropzee-input')">Get Image (as blob / data-url)</button> --}}




    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Enter Site Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="frmManageSite" method="POST" action="{{url("/admin/site/save")}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Site Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Site Name" value="{{isset($site->name) ? $site->name : ""}}">
                                    <input type="hidden" class="form-control" id="id" name="id" value="{{isset($site->id) ? $site->id : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="url">Site Url</label>
                                    <input type="text" class="form-control" id="url" name="url"  placeholder="Enter Site Url" value="{{isset($site->url) ? $site->url : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="site_admin_email">Site Admin Email</label>
                                    <input type="email" class="form-control" id="site_admin_email" name="site_admin_email"  placeholder="Enter Site Admin Email" value="{{isset($site->site_admin_email) ? $site->site_admin_email : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="url">Company Short Description</label>
                                    <textarea class="form-control" id="company_short_description" name="company_short_description"  placeholder="Enter Company short description">{{isset($site->company_short_description) ? $site->company_short_description : ""}}</textarea>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="url">Privacy Policy Footer Text</label>
                                    <textarea class="form-control" id="privacy_policy" name="privacy_policy"  placeholder="Enter Privacy Policy Text" >{{isset($site->privacy_policy) ? $site->privacy_policy : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="url">Company Address</label>
                                    <textarea class="form-control" id="company_address" name="company_address"  placeholder="Enter Company Address">{{isset($site->company_address) ? $site->company_address : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="url">Facebook</label>
                                    <input type="text" class="form-control" id="facebook_link" name="facebook_link"  placeholder="Enter Facebook Url" value="{{isset($site->facebook_link) ? $site->facebook_link : ""}}">
                                </div>
                                <div class="form-group" >
                                    <label for="url" >Twitter</label>
                                    <input type="text" class="form-control" id="twitter_link" name="twitter_link"  placeholder="Enter Twitter Url" value="{{isset($site->twitter_link) ? $site->twitter_link : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="url">Google</label>
                                    <input type="text" class="form-control" id="google_link" name="google_link"  placeholder="Enter Google Url" value="{{isset($site->google_link) ? $site->google_link : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="url">Linkedin</label>
                                    <input type="text" class="form-control" id="linkedin_link" name="linkedin_link"  placeholder="Enter Linkedin Url" value="{{isset($site->linkedin_link) ? $site->linkedin_link : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="website_title">Website Title</label>
                                    <input type="text" class="form-control" id="website_title" name="website_title"  placeholder="Enter Website Title" value="{{isset($site->website_title) ? $site->website_title : ""}}">
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea class="form-control" id="meta_description" name="meta_description"  placeholder="Enter Meta Description" >{{isset($site->meta_description) ? $site->meta_description : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Meta Keywords</label>
                                    <textarea class="form-control" id="meta_keywords" name="meta_keywords"  placeholder="Enter Meta Keywords" >{{isset($site->meta_keywords) ? $site->meta_keywords : ""}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="favicon">Site Favicon Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">

                                            <input type="file"   id="favicon" name="favicon" accept="image/*">
                                            <input type="hidden" value="" id="favicon" name="favicon">
                                            {{-- <label class="custom-file-label" for="siteLogo">Choose Logo Image</label> --}}
                                        </div>
                                    </div>
                                </div>
                                @if(isset($site->favicon))
                                <div class="form-group">
                                    <img  style="max-height: 30px; max-width:173px;" src="{{asset("uploads/favicon/".$site->favicon).'?'.rand(1,100)}}">
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="exampleInputFile">Site Logo Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">

                                            <input type="file"   id="siteLogo" name="siteLogo" accept="image/*">
                                            <input type="hidden" value="" id="cropSiteLogo" name="cropSiteLogo">
                                            {{-- <label class="custom-file-label" for="siteLogo">Choose Logo Image</label> --}}
                                        </div>
                                    </div>
                                </div>
                                @if(isset($site->logo))
                                <div class="form-group">
                                    <img width="173" height="30" src="{{asset("uploads/logo/".$site->logo).'?'.rand(1,100)}}">
                                </div>
                                @endif
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="button" id="cmdSaveSite" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                    {{-- <label for="siteLogo" class="image-previewer" data-cropzee="siteLogo"></label> --}}
                    {{-- <button onclick="alert(cropzeeGetImage('siteLogo'))">Get Image (as blob / data-url)</button>  --}}
                    <div id="imagePreviewDiv" class="image-previewer" data-cropzee="siteLogo"></div>




                </div>



            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</x-admin-layout>
