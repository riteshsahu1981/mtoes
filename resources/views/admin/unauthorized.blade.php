<x-admin-layout>

    @section('title', $pageTitle)

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-6">
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url("/admin")}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Unauthorized</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">You are not authorized to view this page.</h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

</x-admin-layout>
