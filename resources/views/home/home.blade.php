<x-base-layout>

@section('title', $websiteTitle)
@section('customHead')
  <meta name="keywords" content="{{$metaKeywords}}">
  <meta name="description" content="{{$metaDescription}}">
  
@endsection

<section class="section-98 section-md-110 novi-background" data-preset='{"title":"Content block 1","category":"content","id":"content-block-1"}'>
    <div class="container">
      <h1>Welcome</h1>
      <hr class="divider bg-red">
      <div class="row justify-content-sm-center offset-top-66">
        <div class="col-xl-8">
          <p>Welcome to the website Intense Gym! We hope that you will appreciate our services and opportunities we offer our loyal and potential customers. Here are some of them:</p>
        </div>
      </div>
      <div class="row justify-content-center grid-group-lg offset-top-98">
        <div class="col-md-8 col-lg-4">
          <!-- Icon Box Type 5-->
          <div class="box-icon box-icon-bordered"><span class="novi-icon icon icon-outlined icon-sm icon-dark-filled mdi mdi-account-multiple"></span>
            <h4 class="text-danger offset-top-20">Qualified Coaches</h4>
            <p>Our coaches have years of experience in various types of fitness and sports.</p>
          </div>
        </div>
        <div class="col-md-8 col-lg-4">
          <!-- Icon Box Type 5-->
          <div class="box-icon box-icon-bordered"><span class="novi-icon icon icon-outlined icon-xs icon-dark-filled mdi mdi-thumb-up"></span>
            <h4 class="text-danger offset-top-20">Individual Approach</h4>
            <p>Every client of Intense has a personalized program of training and nutrition.</p>
          </div>
        </div>
        <div class="col-md-8 col-lg-4">
          <!-- Icon Box Type 5-->
          <div class="box-icon box-icon-bordered"><span class="novi-icon icon icon-outlined icon-sm icon-dark-filled mdi mdi-dumbbell"></span>
            <h4 class="text-danger offset-top-20">Modern Fitness Equipment</h4>
            <p>We cooperate with leading fitness equipment suppliers to give you the superior results.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Offers-->
  <section data-preset='{"title":"Carousel 1","category":"content, carousel","reload":true,"id":"carousel-1"}'>
    <div class="owl-carousel owl-carousel-default d-lg-none-owl-dots veil-owl-nav d-lg-owl-nav" data-items="1" data-sm-items="2" data-lg-items="3" data-xl-items="4" data-nav="true" data-dots="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]">
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/home-01-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Bodybuilding</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/home-02-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">fitness</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/home-03-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">crossfit</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/home-04-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">circle studio</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/about-01-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Bodybuilding</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
      <div>
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="schedule.html"><img width="480" height="480" src="{{asset("assets/images/about-02-480x480.jpg")}}" alt=""></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">crossfit</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0"></p><a class="btn offset-top-10 offset-md-top-0 btn-danger" href="schedule.html">free first lesson</a>
          </figcaption>
        </figure>
      </div>
    </div>
  </section>
  <!-- Coaches-->
  <section class="section-98 section-md-110 novi-background" data-preset='{"title":"Team","category":"content, team","reload":false,"id":"team"}'>
    <div class="container">
      <h1>Coaches</h1>
      <hr class="divider bg-danger">
      <div class="row justify-content-sm-center offset-md-top-66">
        <div class="col-md-10 col-xl-12">
          <div class="row">
            <div class="col-md-6 col-xl-3 offset-top-66 offset-sm-top-0">
              <!-- Box Member-->
              <div class="box-member"><img class="img-fluid" src="{{asset("assets/images/user-ryan-jackson-270x270.jpg")}}" alt="">
                <h5 class="font-weight-bold offset-top-20"><a href="about-coach.html">Ryan Jackson</a> <small class="text-danger">CrossFit</small>
                </h5>
                <div class="box-member-caption">
                  <div class="box-member-caption-inner">
                    <ul class="list-inline list-inline-xs">
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-facebook icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-twitter icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-google-plus icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <p class="offset-xl-top-0 text-muted">He has more than 10 years of experience in CrossFit and professional athletics.</p>
            </div>
            <div class="col-md-6 col-xl-3 offset-top-66 offset-md-top-0 offset-xl-top-0">
              <!-- Box Member-->
              <div class="box-member"><img class="img-fluid" src="{{asset("assets/images/user-emily-perkins-270x270.jpg")}}" alt="">
                <h5 class="font-weight-bold offset-top-20"><a href="about-coach.html">Emily Perkins</a> <small class="text-danger">Fitness</small>
                </h5>
                <div class="box-member-caption">
                  <div class="box-member-caption-inner">
                    <ul class="list-inline list-inline-xs">
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-facebook icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-twitter icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-google-plus icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <p class="offset-xl-top-0 text-muted">Emily can teach you all peculiarities of fitness and aerobics in a group or individually.</p>
            </div>
            <div class="col-md-6 col-xl-3 offset-top-66 offset-xl-top-0">
              <!-- Box Member-->
              <div class="box-member"><img class="img-fluid" src="{{asset("assets/images/user-samantha-ruiz-270x270.jpg")}}" alt="">
                <h5 class="font-weight-bold offset-top-20"><a href="about-coach.html">Samantha Ruiz</a> <small class="text-danger">Yoga</small>
                </h5>
                <div class="box-member-caption">
                  <div class="box-member-caption-inner">
                    <ul class="list-inline list-inline-xs">
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-facebook icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-twitter icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-google-plus icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <p class="offset-xl-top-0 text-muted">Our leading yoga coach, Samantha will help you reach full harmony with your body.</p>
            </div>
            <div class="col-md-6 col-xl-3 offset-top-66 offset-xl-top-0">
              <!-- Box Member-->
              <div class="box-member"><img class="img-fluid" src="{{asset("assets/images/user-austin-ortiz-270x270.jpg")}}" alt="">
                <h5 class="font-weight-bold offset-top-20"><a href="about-coach.html">Austin Ortiz</a> <small class="text-danger">Bodybuilding</small>
                </h5>
                <div class="box-member-caption">
                  <div class="box-member-caption-inner">
                    <ul class="list-inline list-inline-xs">
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-facebook icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-twitter icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="novi-icon icon fa fa-google-plus icon-xs icon-circle icon-darkest-filled" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <p class="offset-xl-top-0 text-muted">Austin is not only our most called-for coach, but also a winner of many championships.</p>
            </div>
          </div><a class="offset-top-50 btn btn-danger" href="coaches.html">view all coaches</a>
        </div>
      </div>
    </div>
  </section>
  <!-- Latest Blog Posts-->
  <section class="section-98 section-110 bg-lilac novi-background" data-preset='{"title":"Blog 1","category":"content, blog","reload":false,"id":"blog-1"}'>
    <div class="container">
      <h1>Latest Blog Posts</h1>
      <hr class="divider bg-danger">
      <div class="row justify-content-center">
        <div class="col-md-10 col-lg-6">
          <!-- Post Boxed--><a class="d-block" href="blog-post.html">
            <div class="post post-boxed">
              <!-- Post media-->
              <header class="post-media novi-excluded"><img class="img-fluid" width="570" height="321" src="{{asset("assets/images/post-01-570x321.jpg")}}" alt=""></header>
              <!-- Post content-->
              <section class="post-content text-left novi-excluded">
                <div class="post-tags group-xs"><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Fitness</span><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Bodybuilding</span>
                </div>
                <div class="post-body">
                  <!-- Post Title-->
                  <div class="post-title">
                    <h4>The Dead Deadlift</h4>
                  </div>
                  <div class="post-meta small">
                    <ul class="list-inline list-inline-sm p">
                      <li class="list-inline-item font-italic text-middle">by&nbsp;<span class="text-neon-carrot">John Doe</span></li>
                      <li class="list-inline-item"><span class="text-middle icon-xxs mdi mdi-clock novi-icon"> </span>
                        <time class="font-italic text-middle text-neon-carrot" datetime="2018-01-01">2 days ago</time>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            </div></a>
        </div>
        <div class="col-md-10 col-lg-6 offset-top-66 offset-md-top-30 offset-lg-top-0">
          <!-- Post Boxed--><a class="d-block" href="blog-post.html">
            <div class="post post-boxed">
              <!-- Post media-->
              <header class="post-media novi-excluded"><img class="img-fluid" width="570" height="321" src="{{asset("assets/images/post-02-570x321.jpg")}}" alt=""></header>
              <!-- Post content-->
              <section class="post-content text-left novi-excluded">
                <div class="post-tags group-xs"><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Fitness</span><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Bodybuilding</span>
                </div>
                <div class="post-body">
                  <!-- Post Title-->
                  <div class="post-title">
                    <h4>Beyond GPP: The New Model of Performance Training</h4>
                  </div>
                  <div class="post-meta small">
                    <ul class="list-inline list-inline-sm p">
                      <li class="list-inline-item font-italic text-middle">by&nbsp;<span class="text-neon-carrot">John Doe</span></li>
                      <li class="list-inline-item"><span class="text-middle icon-xxs mdi mdi-clock novi-icon"> </span>
                        <time class="font-italic text-middle text-neon-carrot" datetime="2018-01-01">2 days ago</time>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            </div></a>
        </div>
        <div class="row-spacer d-none d-lg-block offset-top-30"></div>
        <div class="col-md-10 col-lg-6 offset-top-66 offset-md-top-30 offset-lg-top-0">
          <!-- Post Boxed--><a class="d-block" href="blog-post.html">
            <div class="post post-boxed">
              <!-- Post media-->
              <header class="post-media novi-excluded"><img class="img-fluid" width="570" height="321" src="{{asset("assets/images/post-03-570x321.jpg")}}" alt=""></header>
              <!-- Post content-->
              <section class="post-content text-left novi-excluded">
                <div class="post-tags group-xs"><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Fitness</span><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Bodybuilding</span>
                </div>
                <div class="post-body">
                  <!-- Post Title-->
                  <div class="post-title">
                    <h4>5 Elements That Belong to Every Athlete’s Training Program</h4>
                  </div>
                  <div class="post-meta small">
                    <ul class="list-inline list-inline-sm p">
                      <li class="list-inline-item font-italic text-middle">by&nbsp;<span class="text-neon-carrot">John Doe</span></li>
                      <li class="list-inline-item"><span class="text-middle icon-xxs mdi mdi-clock novi-icon"> </span>
                        <time class="font-italic text-middle text-neon-carrot" datetime="2018-01-01">2 days ago</time>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            </div></a>
        </div>
        <div class="col-md-10 col-lg-6 offset-top-66 offset-md-top-30 offset-lg-top-0">
          <!-- Post Boxed--><a class="d-block" href="blog-post.html">
            <div class="post post-boxed">
              <!-- Post media-->
              <header class="post-media novi-excluded"><img class="img-fluid" width="570" height="321" src="{{asset("assets/images/post-04-570x321.jpg")}}" alt=""></header>
              <!-- Post content-->
              <section class="post-content text-left novi-excluded">
                <div class="post-tags group-xs"><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Fitness</span><span class="label-custom label-lg-custom label-neon-carrot text-regular font-italic text-capitalize">Bodybuilding</span>
                </div>
                <div class="post-body">
                  <!-- Post Title-->
                  <div class="post-title">
                    <h4>How to Maintain a Perfect Muscular Body After Working Such Out</h4>
                  </div>
                  <div class="post-meta small">
                    <ul class="list-inline list-inline-sm p">
                      <li class="list-inline-item font-italic text-middle">by&nbsp;<span class="text-neon-carrot">John Doe</span></li>
                      <li class="list-inline-item"><span class="text-middle icon-xxs mdi mdi-clock novi-icon"> </span>
                        <time class="font-italic text-middle text-neon-carrot" datetime="2018-01-01">2 days ago</time>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            </div></a>
        </div>
        <div class="col offset-top-66"><a class="btn btn-danger" href="blog.html">view all blog posts</a></div>
      </div>
    </div>
  </section>
  <!-- Testimonials-->
  <section data-preset='{"title":"Testimonials","category":"content, testimonials, parallax","reload":true,"id":"testimonials"}'>
    <div class="parallax-container" data-parallax-img="{{asset("assets/images/background-05-1920x850.jpg")}}">
      <div class="parallax-content">
        <div class="bg-overlay-gray-darkest context-dark">
          <div class="container section-98 section-md-110">
            <h1>Testimonials</h1>
            <hr class="divider bg-white">
            <div class="row justify-content-sm-center">
              <div class="col-md-8">
                <!-- Testimonials Slider v.3-->
                <div class="owl-carousel owl-carousel-testimonials-2" data-items="1" data-nav="true" data-dots="false" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]">
                  <div>
                    <blockquote class="quote quote-slider-3">
                      <p class="quote-body">I came here to acquire the shape of my body I had half a year ago. I was quite surprised with a range of services they offered me.Moreover, you can benefit from their group training.</p><img class="quote-img rounded-circle" width="80" height="80" src="{{asset("assets/images/user-alisa-milano-80x80.jpg")}}" alt="Alisa Milano">
                      <p class="font-weight-bold quote-author">
                        <cite class="text-normal">Alisa Milano</cite>
                      </p>
                      <p class="quote-desc text-gray">Designer at Viper Ltd.</p>
                    </blockquote>
                  </div>
                  <div>
                    <blockquote class="quote quote-slider-3">
                      <p class="quote-body">I am glad I’ve purchased a template from TemplateMonster. Extraordinary customer service, always available, perfect care and knowledge of the technicians. Essential if you are not an expert programmer. In any case highly recommended. Congratulations!</p><img class="quote-img rounded-circle" width="80" height="80" src="{{asset("assets/images/user-july-mao-80x80.jpg")}}" alt="July Mao">
                      <p class="font-weight-bold quote-author">
                        <cite class="text-normal">July Mao</cite>
                      </p>
                      <p class="quote-desc text-gray">Designer at Viper Ltd.</p>
                    </blockquote>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</x-base-layout>
