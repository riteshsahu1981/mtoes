<header class="page-head slider-menu-position" data-preset='{"title":"Header with slider","category":"header, slider","reload":true,"id":"header-1"}'>
    <!-- RD Navbar Transparent-->
    <div class="rd-navbar-wrap">
      <nav class="rd-navbar container rd-navbar-floated rd-navbar-dark" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
        <div class="rd-navbar-inner">
          <!-- RD Navbar Top Panel-->
          {{-- <div class="rd-navbar-top-panel context-dark bg-danger">
             <div class="left-side">
              <address class="contact-info text-left"><a href="tel:#"><span class="icon mdi mdi-cellphone-android novi-icon"></span><span class="text-middle">1-800-1324-567</span></a></address>
            </div>
            <div class="center">
              <address class="contact-info text-left"><a href="#"><span class="icon mdi mdi-map-marker-radius novi-icon"></span><span class="text-middle">2130 Fulton Street San Francisco, CA 94117-1080 USA</span></a></address>
            </div>
            <div class="right-side">
              <ul class="list-inline list-inline-sm">
                <li class="list-inline-item"><a class="novi-icon fa fa-facebook" href="#"></a></li>
                <li class="list-inline-item"><a class="novi-icon fa fa-twitter" href="#"></a></li>
                <li class="list-inline-item"><a class="novi-icon fa fa-google-plus" href="#"></a></li>
                <li class="list-inline-item"><a class="novi-icon fa fa-youtube" href="#"></a></li>
              </ul>
            </div>
          </div> --}}
          <!-- RD Navbar Panel -->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
            <!-- RD Navbar Top Panel Toggle-->
            <button class="rd-navbar-top-panel-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel"><span></span></button>
            <!--Navbar Brand-->
            <div class="rd-navbar-brand" style="position: absolute;margin-top:15px;" ><a href="{{url('/')}}"><img width='173' height='30' src='{{$logo}}' alt=''/></a></div>
          </div>
          <div class="rd-navbar-menu-wrap">
            <div class="rd-navbar-nav-wrap">
              <div class="rd-navbar-mobile-scroll">
                <!--Navbar Brand Mobile-->
                <div class="rd-navbar-mobile-brand"><a href="{{url('/')}}"><img width='173' height='30' src='{{$logo}}' alt=''/></a></div>
                <div class="form-search-wrap">
                  <!-- RD Search Form-->
                  <form class="form-search rd-search" action="search-results.html" method="GET">
                    <div class="form-group">
                      <label class="form-label form-search-label form-label-sm" for="rd-navbar-form-search-widget">Search</label>
                      <input class="form-search-input input-sm form-control form-control-gray-lightest input-sm" id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off">
                    </div>
                    <button class="form-search-submit" type="submit"><span class="mdi mdi-magnify novi-icon"></span></button>
                  </form>
                </div>
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <li  @if($selectedMenuItem == "home")class="active" @endif ><a href="{{url('/')}}"><span>Home</span></a>
                  </li>

                  <li @if($selectedMenuItem == "aboutus")class="active" @endif ><a href="{{url('/page/aboutus')}}"><span>About Us</span></a></li>
                  <li @if($selectedMenuItem == "contactus")class="active" @endif ><a href="{{url('/page/contactus')}}"><span>Contact Us</span></a></li>
                  <li @if($selectedMenuItem == "faq")class="active" @endif ><a href="{{url('/page/faq')}}"><span>FAQ</span></a></li>
                </ul>
              </div>
            </div>
            <!--RD Navbar Search-->
            {{-- <div class="rd-navbar-search rd-navbar-search-top-panel"><a class="rd-navbar-search-toggle mdi" data-rd-navbar-toggle=".rd-navbar-inner,.rd-navbar-search" href="#"><span></span></a>
              <form class="rd-navbar-search-form search-form-icon-right rd-search" action="search-results.html" method="GET">
                <div class="form-group">
                  <label class="form-label" for="rd-navbar-search-form-input">Type and hit enter...</label>
                  <input class="rd-navbar-search-form-input form-control form-control-gray-lightest" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
                </div>
              </form>
            </div> --}}
          </div>
        </div>
      </nav>
    </div>





    @if($selectedMenuItem == "home")
      <x-banner-slider> </x-banner-slider>
    @else
            <div class="context-dark">

              <!-- Modern Breadcrumbs-->
              <section>
              <div class="parallax-container breadcrumb-modern bg-gray-darkest" data-parallax-img="{{asset('assets/images/background-04-1920x750.jpg')}}">
                  <div class="parallax-content">
                  <div class="container section-top-98 section-bottom-34 section-lg-bottom-66 section-lg-98 section-xl-top-110 section-xl-bottom-41">
                      <h2 class="d-none d-lg-block offset-top-30"><span class="big">{{$pageName}}</span></h2>
                      <ul class="list-inline list-inline-dashed">
                      <li class="list-inline-item"><a href="{{url('/')}}">Home</a></li>
                      <li class="list-inline-item">{{$pageName}}</li>
                      </ul>
                  </div>
                  </div>
              </div>
              </section>
            </div>

      @endif


  </header>
