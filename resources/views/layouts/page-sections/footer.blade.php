<footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark novi-background" data-preset='{"title":"Footer","category":"footer","reload":true,"id":"footer"}'>
    <div class="container">
      <div class="row justify-content-md-center text-xl-left">
        <div class="col-md-12">
          <div class="row justify-content-sm-center">
            <div class="col-sm-10 col-md-3 text-left order-md-4 col-md-10 col-xl-3 offset-md-top-50 offset-xl-top-0 order-xl-2">
              <!-- Twitter Feed-->
              {{-- <p class="text-uppercase text-spacing-60 font-weight-bold text-center text-xl-left">Twitter Feed</p> --}}
              <div class="offset-top-20">
                      {!! $currentSite->company_short_description !!}
              </div>
            </div>
            {{-- <div class="col-sm-10 col-md-3 offset-top-66 order-md-3 col-md-10 col-xl-2 offset-xl-top-0 order-xl-3">
              <!-- Flickr Feed-->
              <p class="text-uppercase text-spacing-60 font-weight-bold">Flickr</p>
              <div class="offset-top-24">
                      <div class="group-xs flickr widget-flickrfeed" data-lightgallery="group" data-flickr-tags="tm58888_landscapes"><a class="flickr-item thumbnail-classic" data-lightgallery="item" href="" data-image_c="href" data-size="800x800" data-type="flickr-item"><img width="82" height="82" data-title="alt" src="{{asset("assets/images/_blank.png")}}" alt="" data-image_q="src"></a><a class="flickr-item thumbnail-classic" data-lightgallery="item" href="" data-image_c="href" data-size="800x800" data-type="flickr-item"><img width="82" height="82" data-title="alt" src="{{asset("assets/images/_blank.png")}}" alt="" data-image_q="src"></a><a class="flickr-item thumbnail-classic" data-lightgallery="item" href="" data-image_c="href" data-size="800x800" data-type="flickr-item"><img width="82" height="82" data-title="alt" src="{{asset("assets/images/_blank.png")}}" alt="" data-image_q="src"></a><a class="flickr-item thumbnail-classic" data-lightgallery="item" href="" data-image_c="href" data-size="800x800" data-type="flickr-item"><img width="82" height="82" data-title="alt" src="{{asset("assets/images/_blank.png")}}" alt="" data-image_q="src"></a>
                      </div>
              </div>
            </div> --}}
            <div class="col-sm-10 col-md-3 offset-top-66 order-md-2 offset-md-top-0 col-md-6 col-xl-4 order-xl-4">
              <div class="inset-xl-left-20">
                <p class="text-uppercase text-spacing-60 font-weight-bold">Address</p>
                {{-- <p class="offset-top-20 text-left">
                  Keep up with our always upcoming  product
                  features and technologies. Enter your e-mail and
                  subscribe to our newsletter.
                </p> --}}
                <div class="offset-top-30">
                        {{-- <form class="rd-mailform" data-form-output="form-subscribe-footer" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                          <div class="form-group">
                            <div class="input-group input-group-sm"><span class="input-group-prepend"><span class="input-group-text input-group-icon"><span class="mdi mdi-email novi-icon"></span></span></span>
                              <input class="form-control" placeholder="Type your E-Mail" type="email" name="email" data-constraints="@Required @Email"><span class="input-group-append">
                                <button class="btn btn-sm btn-danger" type="submit">Subscribe</button></span>
                            </div>
                          </div>
                          <div class="form-output" id="form-subscribe-footer"></div>
                        </form> --}}
                        <p>{!! $currentSite->company_address !!}</p>
                </div>
              </div>
            </div>
            <div class="col-sm-10 col-md-3 offset-top-66 order-md-1 offset-md-top-0 col-md-6 col-xl-3 order-xl-1">
              <!-- Footer brand-->
              <div class="footer-brand"><a href="../index.html"><img width='173' height='30' src='{{$logo}}' alt=''/></a></div>
              <div class="offset-top-50 text-sm-center text-xl-left">
                      <ul class="list-inline">
                        @if ($currentSite->facebook_link)
                          <li class="list-inline-item"><a class="novi-icon icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled" href="{{$currentSite->facebook_link}}"></a></li>  
                        @endif
                        
                        @if ($currentSite->twitter_link)
                          <li class="list-inline-item"><a class="novi-icon icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled" href="{{$currentSite->twitter_link}}"></a></li>
                        @endif  

                        @if ($currentSite->google_link)
                          <li class="list-inline-item"><a class="novi-icon icon fa fa-google-plus icon-xxs icon-circle icon-darkest-filled" href="{{$currentSite->google_link}}"></a></li>
                        @endif
                        @if($currentSite->linkedin_link)
                          <li class="list-inline-item"><a class="novi-icon icon fa fa-linkedin icon-xxs icon-circle icon-darkest-filled" href="{{$currentSite->linkedin_link}}"></a></li>
                        @endif
                      </ul>
              </div>
              <p class="text-darker offset-top-20">{!! $currentSite->privacy_policy !!}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
