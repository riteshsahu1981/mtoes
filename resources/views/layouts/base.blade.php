<!DOCTYPE html>
<html class="wow-animation" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>@yield('title')</title>
        @yield('customHead')
        
        <meta charset="utf-8">
        <link rel="icon" href="{{$favicon}}" type="image/x-icon" />
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">

        <meta name="date" content="Dec 26">

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,700,700italic">
        <link rel="stylesheet" href="{{asset("assets/css/style.css")}}">
        <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
        @yield('customCss')
    </head>

     <body>

    <!-- IE Panel-->
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="{{asset("assets/images/ie8-panel/warning_bar_0000_us.jpg")}}" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <!-- Page-->
    <div class="page text-center">

        <!-- header area start -->
        @include('layouts.page-sections.header')
        <!-- header area end -->


        {{ $slot }}


        <!-- footer area start -->
            @include('layouts.page-sections.footer')
        <!-- footer area end -->

        {{-- </div><a class="builder-button" href="https://ld-wp2.template-help.com/novi-builder/intense-gym/" target="_blank"><img src="{{asset("assets/images/novi-icon-26x26.png")}}" width="26" height="26" alt=""><span>Try builder</span></a> --}}
        <!-- Global Mailform Output-->
        <div class="snackbars" id="form-output-global"></div>
        <!-- Java script-->
        <script src="{{asset("assets/js/core.min.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
        @yield('customScript')
    </body>
</html>
