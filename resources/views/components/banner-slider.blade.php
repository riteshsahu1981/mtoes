    @if ($banners->count()==0)
    <div class="context-dark">

        <!-- Modern Breadcrumbs-->
        <section>
        <div class="parallax-container breadcrumb-modern bg-gray-darkest" data-parallax-img="{{asset('assets/images/background-04-1920x750.jpg')}}">
            <div class="parallax-content">
            <div class="container section-top-98 section-bottom-34 section-lg-bottom-66 section-lg-98 section-xl-top-110 section-xl-bottom-41">
                <h2 class="d-none d-lg-block offset-top-30"><span class="big">{{$pageName}}</span></h2>
                <ul class="list-inline list-inline-dashed">
                <li class="list-inline-item"><a href="{{url('/')}}">Home</a></li>
                <li class="list-inline-item">{{$pageName}}</li>
                </ul>
            </div>
            </div>
        </div>
        </section>
      </div>
    @else
        <div class="page-loader page-loader-variant-1">
            <div><img class='img-responsive' style='margin-top: -20px;margin-left: -18px;' width='329' height='67' src='{{asset($logo)}}' alt=''/>
            <div class="offset-top-41 text-center">
                <div class="spinner"></div>
            </div>
            </div>
        </div>

        <!--Swiper-->
        <div class="swiper-container swiper-slider" data-loop="false" data-dragable="false">
        <div class="swiper-wrapper text-center">
            @foreach ($banners as $_banner )
                <div class="swiper-slide" id="page-loader" data-slide-bg="{{asset("uploads/site_banners/".$_banner->banner_image)}}" data-preview-bg="{{asset("uploads/site_banners/".$_banner->banner_image)}} ">
                <div class="swiper-caption swiper-parallax" data-speed="0.5" data-fade="true">
                    <div class="swiper-slide-caption">
                    <div class="container">
                        <div class="row justify-content-xl-center">
                        <div class="col-xl-12">
                            <div class="text-extra-big font-weight-bold font-italic text-uppercase" data-caption-animate="fadeInUp" data-caption-delay="300">{{$_banner->title}}</div>
                        </div>
                        <div class="col-xl-8 offset-top-10">
                            <h5 class="hidden d-sm-block text-light" data-caption-animate="fadeInUp" data-caption-delay="500">
                                {{$_banner->description}}
                            </h5>
                            <div class="offset-top-20 offset-sm-top-50"><a class="btn btn-danger btn-anis-effect" href="#" data-waypoint-to="#welcome" data-caption-animate="fadeInUp" data-caption-delay="800"><span class="btn-text">get started</span></a></div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            @endforeach




        </div>
        <div class="swiper-button swiper-button-prev swiper-parallax mdi mdi-chevron-left"></div>
        <div class="swiper-button swiper-button-next swiper-parallax mdi mdi-chevron-right"></div>
        <div class="swiper-pagination"></div>
        </div>
        <!--Swiper-->
    @endif
