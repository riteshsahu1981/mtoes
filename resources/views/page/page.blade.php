<x-base-layout>

@section('title', $metaTitle)
@section('customHead')
  <meta name="keywords" content="{{$metaKeywords}}">
  <meta name="description" content="{{$metaDescription}}">
@endsection
<section class="section-98 section-md-50 novi-background">
    <div class="container">
        <div class="row justify-content-lg-center"  >
            @if(!is_null($page))
                {!! $page->content !!}
            @else
                <div>The page you are looking for is under maintenance.</div>
                <img src="{{asset("assets/images/maxresdefault.jpg")}}" >
            @endif
        </div>
    </div>
</section>
</x-base-layout>
