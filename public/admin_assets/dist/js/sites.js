$(function () {
  $("#tblSites").DataTable({
    processing : true,
    paging: true,
    serverSide: true,
    ajax: {
      url: "/admin/getSites",
      type: "POST"
    },
    columns: [
      { name: 'id', searchable: false},
      { name: 'name', searchable: true},
      { name: 'url', searchable: true},
      { name: 'logo', searchable: false,  sortable:false},
      { name: 'action', searchable: false, sortable:false}
    ],

    "responsive": true, "lengthChange": false, "autoWidth": false,

  })
//   $('#example2').DataTable({
//     "paging": true,
//     "lengthChange": false,
//     "searching": false,
//     "ordering": true,
//     "info": true,
//     "autoWidth": false,
//     "responsive": true,
//   });





  // $("#cropzee-input").cropzee({startSize: [85, 85, '%']});
  $("#siteLogo").cropzee({
    allowedInputs:['gif','png','jpg','jpeg'],
    aspectRatio:0.17,
    // maxSize:[ 173, 30,  'px'],
    // minSize: [ 173, 30,  'px'],
    startSize: [ 173, 30,  'px'],
    onCropEnd:  function(data) { console.log(data.x, data.y, data.width, data.height); }
  });


  //cropzeeGetImage('siteLogo');


  $('#company_address').summernote({
    focus: true,
    height: 100,
  });
  $('#footer_text').summernote({
    focus: true,
    height: 100,
  });
  $('#company_short_description').summernote({
    focus: true,
    height: 100,
  });
  $('#privacy_policy').summernote({
    focus: true,
    height: 100,
  });

});
$(document).on("click", "#cmdSaveSite", function(){
    console.log(cropzeeGetImage('siteLogo'));
    $("#cropSiteLogo").val(cropzeeGetImage('siteLogo'));
    $("#frmManageSite").submit();
  });
