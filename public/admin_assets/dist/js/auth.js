function myTrim(x){
  return x.replace(/^\s+|\s+$/gm,'');
}
$(function(){
  $("#frmLogin").validate({
      rules: {
        email: {
              required: true
          },
        password: {
              required: true
          },

      },
      messages: {
        email: {
              required: "Please enter email",
          },
        password: {
              required: "Please enter password",
          },
      },
      submitHandler: function(form) {
          // var form = document.getElementById('frmBursary'); //id of form
          var fromData = new FormData();
          fromData.append('email', $("#email").val());
          fromData.append('password', $("#password").val());

          $("#divMessage").hide();
          $("#cmdSavePage").prop('disabled', true);

          $.ajax({
              method: "POST",
              url: "/admin/auth/authenticate",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
              data: fromData,
              dataType: "json",
              cache: false,
              processData: false,  // tell jQuery not to process the data
              contentType: false,  // tell jQuery not to set contentType
              }).done(function(data){
                  console.log(data);
                  $("#cmdSavePage").prop('disabled', false);
                  $("#divMessage").show();
                  $("#divMessage").removeClass();
                  $("#divMessage").html(data.message);
                  if(data.status=="success" ){
                      $("#divMessage").addClass('col-sm-12 alert alert-success');
                      document.location.href = '/admin';
                  }else{
                      $("#divMessage").addClass('col-sm-12 alert alert-danger');
                  }

              });
      }

  });
});
