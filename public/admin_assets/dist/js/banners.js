$(function () {
  var tblBanners;
  function drawBannerTable() {
    tblBanners = $("#tblBanners").DataTable({
      processing: true,
      paging: true,
      serverSide: true,
      ajax: {
        url: "/admin/banners/getBanners",
        type: "POST",
        data: { "site_id": $("#comboSiteId").val() },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content') },
      },
      columns: [
        { name: 'id', searchable: false },
        { name: 'title', searchable: true },
        { name: 'banner_image', searchable: false, sortable: false },
        { name: 'action', searchable: false, sortable: false }
      ],

      "responsive": true, "lengthChange": false, "autoWidth": false,

    });
  }
  drawBannerTable();
  $(document).on("change", "#comboSiteId", function(){
    tblBanners.destroy();
    drawBannerTable();
  });

  $(document).on("click", "#cmdAddBanner", function(){
      document.location.href="/admin/banners/manageBanner/"+$("#comboSiteId").val();
  });

  $(document).on("click", ".cmdDeleteBanner", function(){
    var banner_id=$(this).attr('banner_id');
    
    if(!confirm("Are you sure you want to delete the banner?") ){
      return false;
    }
    $("#cmdDeleteBanner").prop('disabled', true);
    var fromData = new FormData();
    fromData.append('banner_id', banner_id);
    $.ajax({
      method: "POST",
      url: "/admin/banner/delete",
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
      data: fromData,
      dataType: "json",
      cache: false,
      processData: false,  // tell jQuery not to process the data
      contentType: false,  // tell jQuery not to set contentType
      }).done(function(data){
          console.log(data);
          $("#cmdDeleteBanner").prop('disabled', false);
          alert(data.message);
          if(data.status=="success" ){
            $('#comboSiteId').trigger('change');
          } 

      });
    });


  $(document).on("change", "#comboSiteIdManageBanner", function(){

    if($("#banner_id").val()==""){
      document.location.href="/admin/banners/manageBanner/"+$("#site_id").val();
    }else{
      document.location.href="/admin/banners/manageBanner/"+$("#site_id").val()+"/"+$("#banner_id").val();
    }

  });


});
