$(function () {
  var tblUsers;
  function drawUserTable() {
    tblUsers = $("#tblUsers").DataTable({
      processing: true,
      paging: true,
      serverSide: true,
      ajax: {
        url: "/admin/user/getUsers",
        type: "POST",
        data: { "site_id": $("#comboSiteId").val() },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content') },
      },
      columns: [
        { name: 'id', searchable: false, sortable: true },
        { name: 'name', searchable: true, sortable: true },
        { name: 'email', searchable: true, sortable: true },
        { name: 'role_key', searchable: true, sortable: true },
        { name: 'action', searchable: false, sortable: false }
      ],

      "responsive": true, "lengthChange": false, "autoWidth": false,

    });
  }
  drawUserTable();
  $(document).on("change", "#comboSiteId", function(){
    tblUsers.destroy();
    drawUserTable();
  });
  

  $("#frmUser").validate({
    rules: {
      name: {
            required: true
        },
      email: {
            required: true
        },
      site: {
          required: true
      },

    },
    messages: {
      name: {
            required: "Please enter full name",
        },
      email: {
            required: "Please enter email",
        },
      site: {
          required: "Please select site",
      },

    },
    submitHandler: function(form) {
        // var form = document.getElementById('frmBursary'); //id of form
        var fromData = new FormData();
        fromData.append('name', $("#name").val());
        fromData.append('email', $("#email").val());
        fromData.append('password', $("#password").val());
        fromData.append('role_key', $("#role_key").val());
        fromData.append('site', $("#site").val());
        fromData.append('user_id', $("#user_id").val());

        $("#divMessage").hide();
        $("#cmdSavePage").prop('disabled', true);

        $.ajax({
            method: "POST",
            url: "/admin/user/save",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            data: fromData,
            dataType: "json",
            cache: false,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            }).done(function(data){
                console.log(data);
                $("#cmdSavePage").prop('disabled', false);
                $("#divMessage").show();
                $("#divMessage").removeClass();
                $("#divMessage").html(data.message);
                if(data.status=="success" ){
                    $("#user_id").val(data.user_id);
                    $("#divMessage").addClass('col-sm-12 alert alert-success');
                }else{
                    $("#divMessage").addClass('col-sm-12 alert alert-danger');
                }

            });
    }

});


});
