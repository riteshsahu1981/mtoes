function myTrim(x){
  return x.replace(/^\s+|\s+$/gm,'');
}
$('#content').summernote({
  focus: true,
  height: 300,
});
$(function(){

  $("#comboSiteId").on("change", function(){
    document.location.href="/admin/page/"+$("#identifier").val()+"/"+$(this).val();
  });

  $("#frmManagePage").validate({
      rules: {
        content: {
              required: true
          },

      },
      messages: {
        content: {
              required: "Please enter page content",
          },

      },
      submitHandler: function(form) {
          // var form = document.getElementById('frmBursary'); //id of form
          var fromData = new FormData();
          fromData.append('content', $("#content").val());
          fromData.append('meta_description', $("#meta_description").val());
          fromData.append('meta_title', $("#meta_title").val());
          fromData.append('meta_keywords', $("#meta_keywords").val());
          fromData.append('site_id', $("#site_id").val());
          fromData.append('page_id', $("#page_id").val());
          fromData.append('identifier', $("#identifier").val());

          $("#divMessage").hide();
          $("#cmdSavePage").prop('disabled', true);

          $.ajax({
              method: "POST",
              url: "/admin/page/save",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
              data: fromData,
              dataType: "json",
              cache: false,
              processData: false,  // tell jQuery not to process the data
              contentType: false,  // tell jQuery not to set contentType
              }).done(function(data){
                  console.log(data);
                  $("#cmdSavePage").prop('disabled', false);
                  $("#divMessage").show();
                  $("#divMessage").removeClass();
                  $("#divMessage").html(data.message);
                  if(data.status=="success" ){
                      $("#page_id").val(data.page_id);
                      $("#divMessage").addClass('col-sm-12 alert alert-success');
                  }else{
                      $("#divMessage").addClass('col-sm-12 alert alert-danger');
                  }

              });
      }

  });
});
