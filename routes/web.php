<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController as FrontPage;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PageController as AdminPage;
use App\Http\Controllers\Admin\AuthController as AdminAuth;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\UserController as AdminUser;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/page/{identifier?}', [FrontPage::class, 'page']);





Route::get('/admin/login', [AdminAuth::class, 'login'])->name('login');
Route::get('/admin/logout', [AdminAuth::class, 'logout']);
Route::post('/admin/auth/authenticate', [AdminAuth::class, 'authenticate']);

Route::middleware(['auth'])->group(function(){
    Route::get('/admin', [AdminController::class, 'dashboard']);
    Route::get('/admin/sites', [AdminController::class, 'sites']);
    Route::post('/admin/getSites', [AdminController::class, 'getSites']);
    Route::get('/admin/manageSite/{id?}', [AdminController::class, 'manageSite']);
    Route::post('/admin/site/save', [AdminController::class, 'saveSite']);
    Route::get('/admin/header', [AdminController::class, 'header']);
    Route::get('/admin/footer', [AdminController::class, 'footer']);

    Route::get('/admin/page/{identifier?}/{site_id?}', [AdminPage::class, 'page']);
    Route::post('/admin/page/save', [AdminPage::class, 'savePage']);

    Route::get('/admin/banners', [BannerController::class, 'banners']);
    Route::post('/admin/banners/save', [BannerController::class, 'saveSiteBanner']);
    Route::get('/admin/banners/manageBanner/{site_id?}/{id?}', [BannerController::class, 'manageBanner']);
    Route::post('/admin/banners/getBanners', [BannerController::class, 'getBanners']);
    Route::post('/admin/banner/delete', [BannerController::class, 'deleteBanner']);

    Route::get('/admin/users', [AdminUser::class, 'users']);
    Route::post('/admin/user/getUsers', [AdminUser::class, 'getUsers']);
    Route::get('/admin/user/manageUser/{id?}', [AdminUser::class, 'manageUser']);
    Route::post('/admin/user/save', [AdminUser::class, 'saveUser']);


});


