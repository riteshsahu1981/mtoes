<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Role;
use App\Models\Resource;
use App\Models\Acl;
class AddDefaultAcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resource=Resource::create(['resource_key'=>'site', 'description' => "Site"]);

        $role=Role::create(['role_key'=>'super_admin', 'description'=>'Super Admin']);
        Acl::create(['role_key'=>$role->role_key, 'resource_key' => $resource->resource_key, 'edit_permission'=>'1', 'add_permission'=>'1', 'delete_permission'=>'1', 'view_permission'=>'1' ]);
        
        $role=Role::create(['role_key'=>'site_admin', 'description'=>'Site Admin']);
        Acl::create(['role_key'=>$role->role_key, 'resource_key' => $resource->resource_key, 'edit_permission'=>'0', 'add_permission'=>'0', 'delete_permission'=>'0', 'view_permission'=>'0' ]);
    }   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
