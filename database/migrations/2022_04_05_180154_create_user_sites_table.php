<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Site;
use App\Models\User;
use App\Models\UserSite;
class CreateUserSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sites', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('site_id');
        });

        $user=User::create(['email'=>"abc@example.com",
            'name'=>"John",
            'password'=>"12345",
            'role_key'=>'site_admin']);

        $site=Site::create(['name'=>'Site 1',
                'url'=>'http://site1.localhost.com'
        ]);
        UserSite::create(['user_id'=>$user->id, 'site_id'=>$site->id]);

        $site=Site::create(['name'=>'Site Two',
                'url'=>'http://site2.localhost.com'
        ]);
        UserSite::create(['user_id'=>$user->id, 'site_id'=>$site->id]);

        $site=Site::create(['name'=>'Site Three',
                'url'=>'http://site3.localhost.com'
        ]);
        UserSite::create(['user_id'=>$user->id, 'site_id'=>$site->id]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sites');
    }
}
