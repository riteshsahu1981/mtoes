<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class CreateSuperAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role_key')->after('email');
        });
        User::create(['email'=>env('SUPER_ADMIN_EMAIL'),
        'name'=>env('SUPER_ADMIN_NAME'),
        'password'=>env('SUPER_ADMIN_PASSWORD'),
        'role_key'=>'super_admin']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role_key');
        });
        User::where('email', env('SUPER_ADMIN_EMAIL'))->delete();
    }
}
