<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Role;
use App\Models\Resource;
use App\Models\Acl;

class CreateResourceUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resource=Resource::create(['resource_key'=>'user', 'description' => "User"]);
        Acl::create(['role_key'=>'super_admin', 'resource_key' => $resource->resource_key, 'edit_permission'=>'1', 'add_permission'=>'1', 'delete_permission'=>'1', 'view_permission'=>'1' ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Acl::where(['role_key'=>'super_admin', 'resource_key'=>'user'])->delete();
    }
}
